import express, { Express, Request, Response } from 'express'
import * as dotenv from 'dotenv'
dotenv.config()
const port = process.env.PORT || 9000

const app: Express = express()

app.get('/', (req: Request, res: Response) => {
	res.status(200).json({
		status: 'SUCCESS',
		data: {
			api: 'Ahihi'
		}
	})
})

app.listen(port, () => {
	console.log(`⚡️[server]: Server is running at http://localhost:${port}`)
})

